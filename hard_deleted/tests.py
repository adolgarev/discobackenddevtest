from datetime import datetime, timedelta

from django.test import TestCase

from .models import TestHardDelete, empty_trash


class HardDeletedTest(TestCase):
    def setUp(self):
        self.t1 = TestHardDelete(data="instance 1")
        self.t1.save()
        self.t2 = TestHardDelete(data="instance 2")
        self.t2.save()

    def test_delete_set(self):
        self.t1.delete()
        t1 = TestHardDelete.objects.filter(data="instance 1").first()
        self.assertTrue(t1.deleted)

    def test_empty_trash(self):
        self.t2.delete()
        cutoff = datetime.now() + timedelta(0, 1)
        self.assertTrue(empty_trash(cutoff=cutoff))
        self.assertRaises(TestHardDelete.DoesNotExist, TestHardDelete.objects.filter(data="instance 2").get)
        self.assertFalse(empty_trash(cutoff=cutoff))
