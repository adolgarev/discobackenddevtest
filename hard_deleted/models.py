from datetime import datetime, timedelta

from django.db import models
from django.conf import settings
from django.db import transaction


class WithoutHardDeletedManager(models.Manager):
    def get_queryset(self):
        return super(WithoutHardDeletedManager, self).get_queryset().filter(hard_deleted__isnull=True)


class SoftDeleteMixin(models.Model):
    deleted = models.DateTimeField(blank=True, null=True)
    hard_deleted = models.DateTimeField(blank=True, null=True)
    objects = WithoutHardDeletedManager()

    def delete(self, *args, **kwargs):
        self.deleted = datetime.now()
        self.save(update_fields=['deleted'])

    class Meta:
        abstract = True


@transaction.atomic
def empty_trash(cutoff=None):
    if cutoff is None:
        cutoff = datetime.now() - timedelta(days=settings.TRASH_DAYS)
    models = (
        TestHardDelete,
    )
    did_work = False
    for model in models:
        pks = model.objects.select_for_update()\
            .filter(deleted__isnull=False)\
            .filter(deleted__lt=cutoff)\
            .filter(hard_deleted__isnull=True)\
            .values_list('pk', flat=True)[:settings.TRASH_BATCH_SIZE]
        did_work |= (pks.count() > 0)
        model.objects.filter(pk__in=pks).update(hard_deleted=datetime.now())
    return did_work


class TestHardDelete(SoftDeleteMixin, models.Model):
    data = models.CharField(max_length=200)
