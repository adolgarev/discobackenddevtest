from django.apps import AppConfig


class HardDeletedConfig(AppConfig):
    name = 'hard_deleted'
