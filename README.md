# The Task

Slow request middleware (easy, 15 mins)
Write Django middleware to log errors for requests that take more than n seconds.

Stop hard deleting content (easy, 15 mins)
With the following code, add a "hard_deleted" DateTimeField to SoftDeleteMixin, and modify the empty_trash function so that it sets hard_deleted to the current date/time rather than deleting objects.

Extract video thumbnail (medium, ~1 hour)
Write a Python function that takes a filename (of a video file), extracts the last non-blank frame from the video, writes it to a temporary location and returns the filename of the temporary file. Come up with your own definition for "non-blank frame" and explain it. You can use third party libraries, provide a requirements.txt file in your response.
Extra challenge - suggest how you might handle fades to black at the end of the video.


# Requirements & install

* python 3
* pip install -r requirements.txt
* python manage.py migrate
* for video thumbnail task you'll need ffmpeg and imagemagick

# Test

python manage.py test

python manage.py video_thumbnail video_thumbnail/bbb_trailer_1080p.mp4
