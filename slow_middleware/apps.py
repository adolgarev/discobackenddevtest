from django.apps import AppConfig


class SlowMiddlewareConfig(AppConfig):
    name = 'slow_middleware'
