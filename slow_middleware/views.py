import time

from django.http import HttpResponseBadRequest, HttpResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def test(request):
    if request.method != "POST" or request.POST['seconds'] is None:
        return HttpResponseBadRequest()
    seconds = int(request.POST['seconds'])
    time.sleep(seconds)
    return HttpResponse("Slept for {} seconds".format(seconds))
