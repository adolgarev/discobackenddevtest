from mock import patch
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.conf import settings


class SlowMiddlewareTest(TestCase):

    @patch('slow_middleware.middleware.slow_middleware.logger')
    def test_error_logged(self, mock_logger):
        self.client.post(reverse('slow_middleware:test'), {'seconds': settings.LOG_REQUESTS_LONGER_THAN_SECONDS + 1})
        mock_logger.error.assert_called()

    @patch('slow_middleware.middleware.slow_middleware.logger')
    def test_error_not_logged(self, mock_logger):
        self.client.post(reverse('slow_middleware:test'), {'seconds': settings.LOG_REQUESTS_LONGER_THAN_SECONDS - 1})
        mock_logger.error.assert_not_called()
