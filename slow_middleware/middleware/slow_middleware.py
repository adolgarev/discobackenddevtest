from datetime import datetime, timedelta
import logging

from django.conf import settings


logger = logging.getLogger(__name__)


class SlowMiddleware(object):

    def process_request(self, request):
        request._request_start_time = datetime.now()

    def process_response(self, request, response):
        request._response_time = datetime.now() - request._request_start_time
        if request._response_time > timedelta(0, settings.LOG_REQUESTS_LONGER_THAN_SECONDS):
            logger.error("{} took {}".format(request, request._response_time))
        return response
