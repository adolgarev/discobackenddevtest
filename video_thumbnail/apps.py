from django.apps import AppConfig


class VideoThumbnailConfig(AppConfig):
    name = 'video_thumbnail'
