import os
import subprocess
import tempfile
import logging

from django.core.management.base import BaseCommand
from django.conf import settings


logger = logging.getLogger(__name__)


if settings.VIDEO_THUMBNAIL_DEBUG:
    STDERR = None
else:
    STDERR = subprocess.DEVNULL


def get_number_of_frames(video_filename):
    cmd = 'ffprobe -v error -select_streams v:0 -show_entries stream=nb_frames -of default=noprint_wrappers=1:nokey=1'.split(' ')
    cmd.append(video_filename)
    logger.debug("Execute {}".format(cmd))
    return int(subprocess.check_output(cmd, stderr=STDERR))


def extract_frame(input_video_filename, frame_number, output_frame_filename):
    cmd = ['ffmpeg', '-i', input_video_filename, '-vf', "select='eq(n,{})'".format(frame_number), '-vframes', '1',
           output_frame_filename]
    logger.debug("Execute {}".format(cmd))
    subprocess.call(cmd, stderr=STDERR)


def get_number_of_unique_colors(image_filename):
    cmd = 'convert -define histogram:unique-colors=true -format %c histogram:info:-'.split(' ')
    cmd.insert(1, image_filename)
    convert_process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=STDERR)
    count_process = subprocess.Popen("wc -l", shell=True, stdin=convert_process.stdout, stdout=subprocess.PIPE,
                                     stderr=STDERR)
    convert_process.stdout.close()
    logger.debug("Execute {}".format(cmd))
    return int(count_process.communicate()[0])


def is_frame_blank(image_filename):
    # A bit of heuristics
    # Non-blank frame here is a frame with more than VIDEO_THUMBNAIL_COLORS_TO_NOT_BE_BLANK
    # unique colors, that is filter out black, white, fades to black, etc.
    # One can analyse image histogram further to improve blank frame detection algorithm such as
    # do not count white, gray and black colors (colors with R == G == B components), etc.
    return get_number_of_unique_colors(image_filename) < settings.VIDEO_THUMBNAIL_COLORS_TO_NOT_BE_BLANK


def video_thumbnail(input_video_filename):
    frames = get_number_of_frames(input_video_filename)
    max_frames_to_analyse = settings.VIDEO_THUMBNAIL_MAX_FRAMES_TO_ANALYSE
    output_frame_tempfile = tempfile.NamedTemporaryFile(delete=False, suffix=".png")
    output_frame_tempfile.close()
    for i in range(frames - 1, max(frames - max_frames_to_analyse, 0), -1):
        logger.debug("Analysing frame {}".format(i))
        if os.path.exists(output_frame_tempfile.name):
            os.unlink(output_frame_tempfile.name)
        extract_frame(input_video_filename, i, output_frame_tempfile.name)
        if not is_frame_blank(output_frame_tempfile.name):
            break
    return output_frame_tempfile.name


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('input', nargs=1, help="input filename")

    def handle(self, *args, **kwargs):
        print(video_thumbnail(kwargs['input'][0]))
