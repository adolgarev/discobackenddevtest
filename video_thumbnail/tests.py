import os
from django.test import TestCase

from .management.commands.video_thumbnail import (get_number_of_frames,
                                                  get_number_of_unique_colors,
                                                  is_frame_blank,
                                                  video_thumbnail)


class VideoThumbnailTest(TestCase):
    def setUp(self):
        self.video = "video_thumbnail/bbb_trailer_1080p.mp4"
        self.image1 = "video_thumbnail/frame811.png"
        self.image2 = "video_thumbnail/frame800.png"

    def test_get_number_of_frames(self):
        self.assertEqual(get_number_of_frames(self.video), 812)

    def test_get_number_of_unique_colors(self):
        self.assertLess(get_number_of_unique_colors(self.image1), 15)
        self.assertGreater(get_number_of_unique_colors(self.image2), 1000)

    def test_is_frame_blank(self):
        self.assertTrue(is_frame_blank(self.image1))

    def test_is_frame_not_blank(self):
        self.assertFalse(is_frame_blank(self.image2))

    def test_video_thumbnail(self):
        thumbnail_filename = video_thumbnail(self.video)
        self.assertTrue(os.path.exists(thumbnail_filename))
        os.unlink(thumbnail_filename)
